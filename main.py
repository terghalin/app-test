#!/usr/bin/env python3
import requests
import json
import logging
import argparse
import os
from prometheus_client import start_http_server, Gauge
from heapq import nlargest
from functools import reduce
from time import time, sleep

logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(levelname)s]:  %(message)s')

metric_price_spread = Gauge('price_spread', 'Price Spread', ['symbol'])
metric_abs_delta = Gauge('price_spread_delta', 'Price Spread absolute delta', ['symbol'])

def request_data(endpoint: str):
    """
    Request data from remote API endpoint

    :param endpoint: Root API server url

    :return: data
    """
    response = requests.get(endpoint)

    data = response.json() if response and response.status_code == 200 else None
    
    return data

def get_symbols_by_quote(api_url: str, quote_asset):
    """
    Get symbols by quoteAsset

    :param api_url: API endpoint
    :param quote_asset: Quote Asset

    :return: symbols
    """
    data = request_data(api_url + '/api/v3/exchangeInfo')

    symbols = []

    symbols_exchange = data['symbols']
    for symbol in symbols_exchange:
        if symbol['quoteAsset'] == quote_asset:
            symbol_quote = {'symbol':symbol['symbol'], 'quoteAsset':symbol['quoteAsset']}
            symbols.append(symbol_quote)

    return symbols

def get_symbols_ticker(api_url: str):
    """
    Get symbols with trade data

    :param api_url: API endpoint

    :return: symbols
    """
    symbols_ticker = request_data(api_url + '/api/v3/ticker/24hr')

    symbols = []

    for symbol in symbols_ticker:
            symbol_ticker = {
                'symbol':symbol['symbol'], 'volume':symbol['volume'], 
                'count':symbol['count'], 'bidPrice':symbol['bidPrice'], 
                'askPrice':symbol['askPrice']
                }
            symbols.append(symbol_ticker)

    return symbols

def calc_symbols_tnv(api_url: str, symbols: list):
    """
    Calculate Total Notional Value for symbols

    :param api_url: API endpoint
    :param symbols: List of symbols

    :return: symbols
    """
    symbols = symbols

    for symbol in symbols:
        symbol_depth = request_data(api_url + '/api/v3/depth?symbol={}&limit=500'.format(symbol['symbol']))
        
        bids = sorted(symbol_depth['bids'], key=lambda x: float(x[1]))
        asks = sorted(symbol_depth['asks'], key=lambda x: float(x[1]))
        total_bids, total_asks = 0, 0

        for b in bids[:200]:
            logging.debug('Symbol: {}, bids: {}'.format(symbol['symbol'], b))
            notional_value = reduce(lambda x, y: x*y, list(map(float, b)))
            logging.debug("Notional value: {}".format(notional_value))
            total_bids +=notional_value

        for a in asks[:200]:
            logging.debug('Symbol: {}, asks: {}'.format(symbol['symbol'], a))
            notional_value = reduce(lambda x, y: x*y, list(map(float, a)))
            logging.debug("Notional value: {}".format(notional_value))
            total_asks +=notional_value

        logging.debug('Symbol: {}, Total Notional Value (bids): {:20.20f}'.format(symbol['symbol'],total_bids))
        logging.debug('Symbol: {}, Total Notional Value (asks): {:20.20f}'.format(symbol['symbol'],total_asks))
        
        
        symbol['bids_totalNotionalValue'] = '{:20.20f}'.format(total_bids)
        symbol['asks_totalNotionalValue'] = '{:20.20f}'.format(total_asks)
     
    return symbols

def calc_highest_symbols_volume(symbols_volume: list, symbols_quote: list, count):
    """
    Calculate highest symbols volume

    :param symbols_volume: List of symbols by volume
    :param symbols_quote: List of symbols by quoteAsset
    :param count: Number of top symbols

    :return: result
    """
    symbols = [a.copy() for a in symbols_volume]
    [a.update(b) for a, b in zip(symbols, symbols_quote)]
    
    keys = {"symbol", "volume", "quoteAsset"}

    filtered_symbols = []
    for s in symbols:
        if all(key in s for key in keys):
            filtered_symbols.append(s)

    result = nlargest(count, filtered_symbols, key=lambda item: item["volume"])

    return result

def get_spread_and_highest_trades(symbols_volume: list, symbols_quote: list, count):
    """
    Get spread and highest trades

    :param symbols_volume: List of symbols by volume
    :param symbols_quote: List of symbols by quoteAsset
    :param count: Number of top symbols

    :return: result
    """
    symbols = [a.copy() for a in symbols_volume]
    [a.update(b) for a, b in zip(symbols, symbols_quote)]
    
    keys = {"symbol", "count", "quoteAsset", "bidPrice", "askPrice"}

    filtered_symbols = []
    for s in symbols:
        bid = float(s['bidPrice'])
        ask = float(s['askPrice'])

        price_spread = '{:.8f}'.format(abs(bid - ask))
        s['priceSpread'] = price_spread

        if all(key in s for key in keys):
            filtered_symbols.append(s)

    result = nlargest(count, filtered_symbols, key=lambda item: item["count"])

    return result

def setup_metric(symbol, spread, delta):
    """
    Setup metrics for Prometheus

    :param symbol: Name of symbol
    :param spread: Price spread
    :param delta: Delta of previous price spread
    """
    metric_price_spread.labels(symbol).set(spread)
    metric_abs_delta.labels(symbol).set(delta)


def main():
    """
    Run main routine
    """

    parser = argparse.ArgumentParser(description='run symbols parse')

    required = parser.add_argument_group('Required arguments')
    required.add_argument('--api_url', default='https://api.binance.com', help='API url')

    args = parser.parse_args()

    api_url = args.api_url
    logging.debug('Executing main routine')

    btc_symbols_quote = get_symbols_by_quote(api_url, 'BTC')
    usdt_symbols_quote = get_symbols_by_quote(api_url, 'USDT')
    symbols_volume = get_symbols_ticker(api_url)
    
    top_volume = calc_highest_symbols_volume(symbols_volume, btc_symbols_quote, 5)
    top_trades = get_spread_and_highest_trades(symbols_volume, usdt_symbols_quote, 5)

    logging.info('Q1: Getting Top 5 BTC by volume')
    for symbol in top_volume:
        print('Symbol: {}, Volume: {}'.format(symbol['symbol'], symbol['volume']))

    logging.info('Q2: Getting Top 5 USDT by trades')
    for symbol in top_trades:
        print('Symbol: {}, Trades: {}'.format(symbol['symbol'], symbol['count']))

    logging.info('Q3: Getting Q1 Total Notional Value')
    q3_symbols = calc_symbols_tnv(api_url, top_volume)
    for symbol in q3_symbols:
        print('Symbol: {}, Bids TNV: {}, Asks TNV: {}'.format(symbol['symbol'], symbol['bids_totalNotionalValue'], symbol['asks_totalNotionalValue']))

    logging.info('Q4: Getting Q2 Price Spread')
    for symbol in top_trades:
        print('Symbol: {}, Price Spread: {}'.format(symbol['symbol'], symbol['priceSpread']))

    start_http_server(8080)
    price_spread = 0
    delta = 0

    while True:
        symbols_volume = get_symbols_ticker(api_url)
        top_trades = get_spread_and_highest_trades(symbols_volume, usdt_symbols_quote, 5)

        logging.info('Q5: Getting Q4 Price Spread and Delta')

        for symbol in top_trades: 
            symbol_name = symbol['symbol']
            delta = abs(price_spread - float(symbol['priceSpread']))
     
            print('Symbol: {}, Price Spread: {}, Delta: {:.8f}'.format(symbol['symbol'], symbol['priceSpread'], delta))
            
            setup_metric(symbol_name, price_spread, delta)

            price_spread = float(symbol['priceSpread'])
        
        sleep(10 - time() % 10)

if __name__ == '__main__':
    main()
